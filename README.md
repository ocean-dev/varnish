# Varnish for Ocean

This is the Varnish Docker container for Ocean. It's based on `million12/varnish`,
which it extends for AWS by adding an Nginx proxy behind Varnish before
connecting to the backend. It also uses a custom Varnish VCL configuration file.

This is necessary since load balancers on AWS have dynamic IPs which may change
as the load changes in volume. Varnish needs static IPs to connect to; it doesn't
resolve DNS names for reasons of speed. Therefore, the Nginx proxy, running
locally on the same instance as Varnish, provides such a static IP.

The Nginx proxy resolves the UPSTREAM_ENDPOINT environment variable every 30
seconds, which allows the ALB on AWS to do its thing. When running under Docker
Compose, the UPSTREAM_ENDPOINT simply points to another Nginx instance which
serves as a load balancer for local purposes.


# The VCL Configuration File

Varnish is an integral part of the Ocean programming model, which amongst other
things includes aggressive, very efficient caching. Cf http://wiki.oceanframework.net/Caching_in_Ocean for a full description of
caching in Ocean.

The VCL configuration file configures Varnish to support the non-standard BAN
and PURGE HTTP methods. It also provides support for browser clients, CORS, etc.


# ENV variables

The container accepts the following ENV variables:

  * `PRODUCTION` - true for the `prod` environment, `null` for all others. This
      controls whether extra HTTP headers describing each resources caching
      status are added to outgoing resources, something which is useful in
      development environments, but which should be hidden from clients in
      production.
  * `CACHE_SIZE` - the size of the Varnish cache. The default is `64m`, 64 MB.
  * `VARNISHD_PARAMS` - extra parameters to pass on to the start command for the
      Varnish daemon. Set to `-T 127.0.0.1:6082 -p ban_lurker_sleep=0.1` with
      Docker Compose. Make sure you include at least the ban lurker arg.
  * `ALLOW_PURGE_BAN_FROM_n_CIDR` - used to specify from which IP address ranges
      Varnish should accept PURGE and BAN requests. Under Docker Compose, you
      can use `0.0.0.0/0` which accepts traffic from anywhere. On AWS, you
      should use a CIDR mask coverning only your VPC, such as `10.0.0.0/16`.
      NB: the `n` in the ENV variable name is an index. You can use as many
      ENV variables as you need, starting with an `n` of `0`.
  * `NGINX_PROXY_RESOLVER` - Specifies the resolver of load balancer IPs. Under
      Docker Compose, this is the internal Docker resolver (`127.0.0.11 ipv6=off`).
      On AWS, the resolver is `172.16.0.23`.
  * `UPSTREAM_ENDPOINT` - the backend endpoint to connect to. Under Docker Compose,
      there's another Nginx at `http://host.docker.internal:8088` which acts as
      a load balancer. On AWS, use the URL of the internal ALB.


# Healthcheck

Ocean Varnish defines a healthcheck at `/ping` which returns a HTTP status of
200 and a body of `OK` when Varnish is running. 
