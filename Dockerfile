FROM million12/varnish
LABEL net.oceanframework.project="ocean" \
      net.oceanframework.service="varnish" \
      maintainer="peter@peterbengtson.com" \
      documentation="http://wiki.oceanframework.net"

RUN yum install -y nodejs && \
  npm i -g envhandlebars && \
  yum install -y nginx && \
  yum clean all && \
  rm -rf /var/cache/yum

ADD varnish-default.vcl /etc/varnish/varnish-default.vcl
ADD varnish-nginx.conf /etc/nginx/varnish-nginx.conf

CMD ["/bin/bash", "-c", "envhandlebars < /etc/varnish/varnish-default.vcl > /etc/varnish/default.vcl && envhandlebars < /etc/nginx/varnish-nginx.conf > /etc/nginx/nginx.conf && nginx && /start.sh"]
